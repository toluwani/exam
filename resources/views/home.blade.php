
@include ('layouts.head')


{{-- <body id="home"> --}}

<!-- Wrapper -->
<div class="wrapper">
	
	<!-- Header -->
@include('layouts.nav')

	<!-- Header -->

	<!-- Banner & Enquiry Form -->
	<div class="banner">
		
		<!-- Banner slider -->
		<div id="banner-slider" class="banner-slider">
				
			<!-- item -->
			<div class="slide overlay-dark fullscreen" style="background: url(images/banner/img-03.jpg); background-size: cover;">
				<div class="caption-holder">
					<div class="tc-display-table">
						<div class="tc-display-table-cell">
							<div class="banner-text animate fadeInUp" data-wow-delay="0.4s">
								<h1>   we inspire<span>Education is the movement from darkness to light</span></h1>
								<p>Vivamus tempor felis at nulla dapibus cursus. Donec pretium nulla eu lobortis efficitur. 
								<span>Quisquefeugiat, orci investibulum porta, metus lacus molestie mauris.</span></p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- item -->

			<!-- item -->
			<div class="slide overlay-dark fullscreen" style="background: url(images/banner/hero16.jpg); background-size: cover;">
				<div class="caption-holder">
					<div class="tc-display-table">
						<div class="tc-display-table-cell">
							<div class="banner-text animate fadeInUp" data-wow-delay="0.4s">
								<h1> we are here to help<span>Education is the movement from darkness to light</span></h1>
								<p>Vivamus tempor felis at nulla dapibus cursus. Donec pretium nulla eu lobortis efficitur. 
								<span>Quisquefeugiat, orci investibulum porta, metus lacus molestie mauris.</span></p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- item -->

		</div>
		<!-- Banner slider -->

		<!-- Enquiry Form -->
		<div class="enquiry-form-holder">
			<div class="container">
				<div class="form-btn-holder">
					<div class="bars-icon hide-md"><span>Enquiry</span></div>
					<span class="bars-icon appear-lg" data-toggle="modal" data-target=".bs-example-modal-sm"></span>
				</div>
				<form class="enquiry-form hide-md">
					<div class="enquiry-form-input">
						<input type="text" class="form-control" placeholder="full name">
					</div>
					<div class="enquiry-form-input">
						<input type="text" class="form-control" placeholder="email id">
					</div>
					<div class="enquiry-form-input">
						<input type="text" class="form-control" placeholder="class">
					</div>
					<div class="enquiry-form-input">
						<input type="text" class="form-control" placeholder="subject">
					</div>
					<div class="enquiry-form-input">
						<button class="white-btn" type="submit">submit</button>
					</div>
				</form>
			</div>
		</div>
		<!-- Enquiry Form -->

	</div>
	<!-- Banner & Enquiry Form -->

	<!-- Main Content -->
	<main class="main-content">

		<!-- About The School -->
		<section class="about-school about-school-v2 section-padding-top white-bg">
			<div class="container">
				<div class="row">

					<!-- About To School -->
					<div class="col-sm-7">
						<div class="about-sec section-padding">
							<h2><strong>About </strong>Our  exam portal<span>Lorem ipsum dolor sit amet</span></h2>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas pulvinar tellus sed mauvehicula tempor. In hac habitasse platea dictumst. Donec nunc nunc, interdum sed aliquet quis, conditum vitae enim. Quisque molestie consectetur urna quis scelerisque. Morbi at lectus sapien. Donec fgiat arcu in mi placerat ullamcorper.</p>
							<p>In tempor, ex id viverra convallis, ante augue vestibulum leo, eget elementum risus nulla bibendum nisl. Maecenas lacinia libero tincidunt justo bibendum sagittis. Duis gravida massa ac vehicula pulvinar. Nunc ligula orci, vulputate id tempor ac, tincidunt in orci. Etiam nulla lectus, commodo quis tortor et, congue volutpat velit.</p>
							<a class="pink-btn" href="#">read more</a>
						</div>
					</div>
					<!-- About To School -->

					<!-- About Img Section -->
					<figure class="about-sec-img about-sec-img-v2">
						<img class="animate fadeInRight" data-wow-delay="0.4s" src="images/about-sec-img2.png" alt="about-sec-img2">
					</figure>
					<!-- About Img Section -->

				</div>
			</div>
		</section>
		<!-- About The School -->

		<!-- Services Section -->
		<section class="parallax-window section-padding overlay-pink" data-image-src="images/service-bg-v2.jpg" data-parallax="scroll">
			<div class="container">
					
				<!-- Main Heading -->
				<div class="main-heading-holder">
					<div class="main-heading white-heading">
						<h2>Our School Services<span>Nam commodo enim non eleifend imper</span></h2>
					</div>
				</div>
				<!-- Main Heading -->

				<!-- Services -->
				<div class="service-v2-slider">

					<!-- Service Figure -->
					<div class="service-figure-v2 animate fadeInUp" data-wow-delay="0.2s">
						<span class="service-icon"><i class="icon-man-with-tie"></i></span>
						<h3><a href="#">Experience Teacher</a></h3>
						<p>In tempor, ex id viverra convallis, ante augue vestibulum leo, eget elementum risus nulla bibendum nisl.</p>
					</div>
					<!-- Service Figure -->

					<!-- Service Figure -->
					<div class="service-figure-v2 animate fadeInUp" data-wow-delay="0.4s">
						<span class="service-icon"><i class="icon-college-studying"></i></span>
						<h3><a href="#">All Courses Available</a></h3>
						<p>In tempor, ex id viverra convallis, ante augue vestibulum leo, eget elementum risus nulla bibendum nisl.</p>
					</div>
					<!-- Service Figure -->

					<!-- Service Figure -->
					<div class="service-figure-v2 animate fadeInUp" data-wow-delay="0.6s">
						<span class="service-icon"><i class="icon-swimming-figure"></i></span>
						<h3><a href="#">Other Activities</a></h3>
						<p>In tempor, ex id viverra convallis, ante augue vestibulum leo, eget elementum risus nulla bibendum nisl.</p>
					</div>
					<!-- Service Figure -->

					<!-- Service Figure -->
					<div class="service-figure-v2 animate fadeInUp" data-wow-delay="0.8s">
						<span class="service-icon"><i class="icon-man-with-tie"></i></span>
						<h3><a href="#">Experience Teacher</a></h3>
						<p>In tempor, ex id viverra convallis, ante augue vestibulum leo, eget elementum risus nulla bibendum nisl.</p>
					</div>
					<!-- Service Figure -->

				</div>
				<!-- Services -->

			</div>
		</section>
		<!-- Services Section -->





		<!-- Our School Other Services -->
				<!-- Our School Other Services -->

		<!-- Upcoming Events -->
		<section class="parallax-window section-padding overlay-dark" data-image-src="images/event-bg-2.jpg" data-parallax="scroll">
			<div class="container">
					
				<!-- Main Heading -->
				<div class="main-heading-holder">
					<div class="main-heading white-heading">
						<h2>Upcoming Events<span>Nunc posuere malesuada vulputate</span></h2>
					</div>
				</div>
				<!-- Main Heading -->

				<!-- Upcoming Events -->
				<div class="upcoming-events">
					<div class="row">
							
						<!-- Events Post -->
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 r-full-width">
							<div class="post-holder">
								<div class="row no-gutters">
									
									<!-- Post Img -->
									<div class="col-lg-6 col-sm-12 col-xs-12 r-full-width">
										<figure class="post-img">
											<img src="images/upcomming-events/img-03.jpg" alt="img-01">
										</figure>
									</div>
									<!-- Post Img -->

									<!-- Post Detail -->
									<div class="col-lg-6 col-sm-12 col-xs-12 r-full-width">
										<div class="post-detail">
											<h3><a href="#">Event Title Name</a></h3>
											<ul class="loaction-tags">
												<li><i class="fa fa-map-marker"></i>King Street, Melbourne VIC 3000</li>
												<li><i class="fa fa-clock-o"></i>10:30 am To 8:00 pm</li>
											</ul>
											<p>Lorem ipsum dolor sit amet, contetur adipiscing elit. Sed id tellus in diam tempus euismod.</p>
											<a href="#" class="read-more">Read More</a>
											<ul id="countdown-1" class="countdown">
											  	<li><span class="days">00</span><p>Day</p></li>
												<li><span class="hours">00</span><p>Hour</p></li>
												<li><span class="minutes">00</span><p>min</p></li>
												<li><span class="seconds">00</span><p>sec</p></li>
											</ul>
										</div>
									</div>
									<!-- Post Detail -->

								</div>
								<strong class="date-batch-left">28<span>DEC</span></strong>
							</div>
						</div>
						<!-- Events Post -->

						<!-- Events Post -->
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 r-full-width">
							<div class="post-holder">
								<div class="row no-gutters">
									
									<!-- Post Img -->
									<div class="col-lg-6 col-xs-12 r-full-width">
										<figure class="post-img">
											<img src="images/upcomming-events/img-04.jpg" alt="img-02">
										</figure>
									</div>
									<!-- Post Img -->

									<!-- Post Detail -->
									<div class="col-lg-6 col-xs-12 r-full-width">
										<div class="post-detail">
											<h3><a href="#">Event Title Name</a></h3>
											<ul class="loaction-tags">
												<li><i class="fa fa-map-marker"></i>King Street, Melbourne VIC 3000</li>
												<li><i class="fa fa-clock-o"></i>10:30 am To 8:00 pm</li>
											</ul>
											<p>Lorem ipsum dolor sit amet, contetur adipiscing elit. Sed id tellus in diam tempus euismod.</p>
											<a href="#" class="read-more">Read More</a>
											<ul id="countdown-2" class="countdown">
											  	<li><span class="days">00</span><p>Day</p></li>
												<li><span class="hours">00</span><p>Hour</p></li>
												<li><span class="minutes">00</span><p>min</p></li>
												<li><span class="seconds">00</span><p>sec</p></li>
											</ul>
										</div>
									</div>
									<!-- Post Detail -->

								</div>
								<strong class="date-batch-right">28<span>DEC</span></strong>
							</div>
						</div>
						<!-- Events Post -->

					</div>
				</div>
				<!-- Upcoming Events -->

			</div>
		</section>
		<!-- Upcoming Events -->
		
		<!-- Team Section -->
        
        
		<!-- Our Blog -->

		

		
		

		

		<!-- Pricing Plans -->
		<section class="pricing-holder section-padding white-bg">
			<div class="container">

				<!-- Main Heading -->
				<div class="main-heading-holder">
					<div class="main-heading">
						<h2>Our <strong>Price Table</strong><span>our best services for you</span></h2>
						<p>Vivamus tempor felis at nulla dapibus cursus. Donec pretium nulla eu lobortis efficitur.
						<span>Quisquefeugiat, orci investibulum porta, metus lacus molestie mauris.</span></p>
					</div>
				</div>
				<!-- Main Heading -->

				<!-- Pricing Plans -->
				<div class="row">
				
					<!-- Pricing Column -->
					<div class="col-sm-4">
						<div class="pricing-column">
							<span class="service-icon animate fadeInUp" data-wow-delay="0.4s"><i class="fa fa-pencil"></i></span>
							<h3> per hour</h3>
							<div class="pricing-price">
								<h2>150 naira<span> 1hr</span></h2>
							</div>
							<div class="pricing-content">
								<div class="service-lits">
									<ul>
										<li>Our best services for</li>
										<li>Duis fermentum convallis</li>
										<li>Our best services for</li>
										<li>Duis fermentum convallis</li>
										<li>Our best services for</li>
									</ul>
								</div>
								<a class="pink-btn" href="#">more details</a>
							</div>
						</div>
					</div>
					<!-- Pricing Column -->

					<!-- Pricing Column -->
					<div class="col-sm-4">
						<div class="pricing-column">
							<span class="service-icon animate fadeInUp" data-wow-delay="0.6s"><i class="icon-man-with-tie"></i></span>
							<h3>  24 hrs</h3>
							<div class="pricing-price">
								<h2>150o<span> 1 day</span></h2>
							</div>
							<div class="pricing-content">
								<div class="service-lits">
									<ul>
										<li>Our best services for</li>
										<li>Duis fermentum convallis</li>
										<li>Our best services for</li>
										<li>Duis fermentum convallis</li>
										<li>Our best services for</li>
									</ul>
								</div>
								<a class="pink-btn" href="#">more details</a>
							</div>
						</div>
					</div>
					<!-- Pricing Column -->

					<!-- Pricing Column -->
					<div class="col-sm-4">
						<div class="pricing-column">
							<span class="service-icon animate fadeInUp" data-wow-delay="0.8s"><i class="fa fa-mortar-board"></i></span>
							<h3> A week</h3>
							<div class="pricing-price">
								<h2>15000<span>1 week</span></h2>
							</div>
							<div class="pricing-content">
								<div class="service-lits">
									<ul>
										<li>Our best services for</li>
										<li>Duis fermentum convallis</li>
										<li>Our best services for</li>
										<li>Duis fermentum convallis</li>
										<li>Our best services for</li>
									</ul>
								</div>
								<a class="pink-btn" href="#">more details</a>
							</div>
						</div>
					</div>
					<!-- Pricing Column -->
				
				</div>
				<!-- Pricing Plans -->

			</div>
		</section>
		<!-- Pricing Plans -->

		<!-- Our School Mobile App -->
		<section class="parallax-window section-padding overlay-gray" data-image-src="images/mobile-app-bg.jpg" data-parallax="scroll">
			<div class="container">
					
				<!-- Main Heading -->
				<div class="main-heading-holder">
					<div class="main-heading">
						<h2>Our  <strong>Mobile App</strong></h2>
					</div>
				</div>
				<!-- Main Heading -->

				<!-- App Option -->
				<div class="row">
					
					<!-- About Our App -->
					<div class="col-sm-6">
						<div class="about-apps">
							<h2><strong>About</strong> Our App</h2>
							<ul class="app-list-option">
								<li class="animate fadeInUp" data-wow-delay="0.6s">
									<span class="service-icon"><i class="fa fa-android"></i></span>
									<div class="app-detail">
										<h4>Our Application on Android</h4>
										<p>Vivamus tempor felis at nulla dapibus cursus. Done pretium nulla eu lobortis efficitur Quisquefeugiat.</p>
									</div>
								</li>
								<li class="animate fadeInUp" data-wow-delay="0.8s">
									<span class="service-icon"><i class="fa fa-apple"></i></span>
									<div class="app-detail">
										<h4>Our Application on ios</h4>
										<p>Vivamus tempor felis at nulla dapibus cursus. Done pretium nulla eu lobortis efficitur Quisquefeugiat.</p>
									</div>
								</li>
								<li class="animate fadeInUp" data-wow-delay="1s">
									<span class="service-icon"><i class="fa fa-windows"></i></span>
									<div class="app-detail">
										<h4>Our Application on windows</h4>
										<p>Vivamus tempor felis at nulla dapibus cursus. Done pretium nulla eu lobortis efficitur Quisquefeugiat.</p>
									</div>
								</li>
							</ul>
						</div>
					</div>
					<!-- About Our App -->

					<!-- Mobiles Img -->
					<div class="col-sm-6">
						<figure class="mobile-img">
							<img class="animate fadeInRight" data-wow-delay="0.4s" src="images/mobile-img.png" alt="mobile-img">
						</figure>
					</div>
					<!-- Mobiles Img -->

				</div>
				<!-- App Option -->

			</div>
		</section>
		<!-- Our School Mobile App -->

		

		<!-- Testimonial Section -->
		<section class="testimonial-holder section-padding white-bg">
			<div class="container">

				<!-- Main Heading -->
				<div class="main-heading-holder">
					<div class="main-heading">
						<h2> <strong>Testimonials</strong><span>In gravida ultrices magna in rhoncu</span></h2>
						<p>Vivamus tempor felis at nulla dapibus cursus. Donec pretium nulla eu lobortis efficitur.
						<span>Quisquefeugiat, orci investibulum porta, metus lacus molestie mauris.</span></p>
					</div>
				</div>
				<!-- Main Heading -->

				<!-- Testimonial Slider -->
				<div id="testimonial-slider" class="testimonial-slider">
					
					<!-- Testimonial column -->
					<div class="item animate">
						<div class="testimonial-column">
							<div class="testimonial-blockquote-holder">
								<blockquote class="testimonial-blockquote">
									<p>Nunc ullamcorper augue nec accumsan porta. Ut lacinia fgiat viverra. Ut dictum turpis in ipsum sagittis finibus.</p>
								</blockquote>
								<span class="client-name">Wll Smith</span>
							</div>
							<div class="client-img">
								<img src="images/client-img/img-01.jpg" alt="img-01">
							</div>
						</div>
					</div>
					<!-- Testimonial column -->

					<!-- Testimonial column -->
					<div class="item">
						<div class="testimonial-column">
							<div class="testimonial-blockquote-holder">
								<blockquote class="testimonial-blockquote">
									<p>Nunc ullamcorper augue nec accumsan porta. Ut lacinia fgiat viverra. Ut dictum turpis in ipsum sagittis finibus.</p>
								</blockquote>
								<span class="client-name">Wll Smith</span>
							</div>
							<div class="client-img">
								<img src="images/client-img/img-01.jpg" alt="img-01">
							</div>
						</div>
					</div>
					<!-- Testimonial column -->

					<!-- Testimonial column -->
					<div class="item">
						<div class="testimonial-column">
							<div class="testimonial-blockquote-holder">
								<blockquote class="testimonial-blockquote">
									<p>Nunc ullamcorper augue nec accumsan porta. Ut lacinia fgiat viverra. Ut dictum turpis in ipsum sagittis finibus.</p>
								</blockquote>
								<span class="client-name">Wll Smith</span>
							</div>
							<div class="client-img">
								<img src="images/client-img/img-01.jpg" alt="img-01">
							</div>
						</div>
					</div>
					<!-- Testimonial column -->

				</div>
				<!-- Testimonial Slider -->

			</div>
		</section>
		<!-- Testimonial Section -->

	</main>
	<!-- Main Content -->

	<!-- Subscribe to our Newsletter -->
	<div class="subscribe-now section-padding">
		<div class="container">
			
			<!-- Main Heading -->
			<div class="main-heading-holder">
				<div class="main-heading white-heading">
					<h2>Subscribe to our Newsletter<span>Phasellus ornare odio eget ultrices</span></h2>
				</div>
			</div>
			<!-- Main Heading -->

			<!-- Newsletter -->
			<div class="subscribe-newsletter">
				<div class="subscribe-submit">
					<input class="form-control" type="text" placeholder="Type here">
					<button type="submit" class="white-btn">submit</button>
				</div>
			</div>
			<!-- Newsletter -->

		</div>
	</div>
	<!-- Subscribe to our Newsletter -->

	<!-- Footer -->
	<footer id="footer" class="footer overlay-dark">
		<div class="container">
			
			<!-- Footer Logo Section -->
			<div class="footer-logo-sec">
				<div class="footer-logo-inner">
					<a class="footer-logo" href="#"><img src="images/logo.png" alt="logo"></a>
					<p>Cras pharetra justo sed scelerisque finibus. Vestibulum ante ipsum primis in faucibus
					<span>orci luctus et ultrices posuere cubilia Curae; Etiam luctus.</span></p>
					<ul class="social-icons">
						<li><a href="#"><i class="fa fa-facebook"></i></a></li>
						<li><a href="#"><i class="fa fa-twitter"></i></a></li>
						<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
						<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
						<li><a href="#"><i class="fa fa-skype"></i></a></li>
					</ul>
				</div>
			</div>
			<!-- Footer Logo Section -->

			<!-- Footer Columns -->
			<div class="row">
				<div class="footer-columns">
					
					<!-- Footer Column -->
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 r-full-width">
						<div class="footer-column">
							<h4>Contact :</h4>
							<div class="contact-widget">
								<div class="location">
									<span class="location-icon"><i class="fa fa-map-marker"></i></span>
									<div>
										<p>King Street, Melbourne</p>
										<p>VIC 3000, Australia84</p>
									</div>
								</div>
							</div>
							<div class="contact-widget">
								<div class="location">
									<span class="location-icon"><i class="fa fa-mobile"></i></span>
									<div>
										<p>King Street, Melbourne</p>
										<p>VIC 3000, Australia84</p>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- Footer Column -->

					<!-- Footer Column -->
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 r-full-width">
						<div class="footer-column">
							<h4>Recent Work :</h4>
							<ul class="flicker-img">
								<li><a href="#"><img class="animate fadeInUp" data-wow-delay="0.2s" src="images/flicker-img/img-01.jpg" alt="img-01"></a></li>
								<li><a href="#"><img class="animate fadeInUp" data-wow-delay="0.4s" src="images/flicker-img/img-02.jpg" alt="img-02"></a></li>
								<li><a href="#"><img class="animate fadeInUp" data-wow-delay="0.6s" src="images/flicker-img/img-03.jpg" alt="img-03"></a></li>
								<li><a href="#"><img class="animate fadeInUp" data-wow-delay="0.8s" src="images/flicker-img/img-04.jpg" alt="img-04"></a></li>
								<li><a href="#"><img class="animate fadeInUp" data-wow-delay="1s" src="images/flicker-img/img-05.jpg" alt="img-05"></a></li>
								<li><a href="#"><img class="animate fadeInUp" data-wow-delay="1.2s" src="images/flicker-img/img-06.jpg" alt="img-06"></a></li>
							</ul>
						</div>
					</div>
					<!-- Footer Column -->

					<!-- Footer Column -->
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 r-full-width">
						<div class="footer-column">
							<h4>Time :</h4>
							<ul class="school-timing">
								<li>Praesent vel egestas orci.</li>
								<li>Fusccinia luctus sapien nec vulputate.</li>
								<li> Time</li>
								<li>MON - THU :  8:00 AM To 5:00 PM</li>
								<li>FRI - SAT : 8:00 AM To 2:00 PM</li>
							</ul>
						</div>
					</div>
					<!-- Footer Column -->

					<!-- Footer Column -->
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 r-full-width">
						<div class="footer-column">
							<h4>Location :</h4>
							<div id="location-map" class="location-map"></div>
						</div>
					</div>
					<!-- Footer Column -->

				</div>
			</div>
			<!-- Footer Columns -->

			<!-- Copy Rights -->
			<div class="copy-rights">
				<p>Copyright ©2018 alonge studios</p>
			</div>
			<!-- Copy Rights -->

		</div>
	</footer>
	<!-- Footer -->

</div>
<!-- Wrapper -->

<!-- back To Button -->
<span id="scrollup" class="scrollup"><i class="fa fa-angle-up"></i></span>
<!-- back To Button -->

<!-- Form Modal Box -->
<div class="modal fade bs-example-modal-sm">
  	<div class="modal-dialog modal-sm">
	    <div class="modal-body enquiry-form style-2 white-heading">
	    	<h3>Enquiry Form</h3>
	       <form class="enquiry-form">
				<div class="enquiry-form-input">
					<input type="text" class="form-control" placeholder="full name">
				</div>
				<div class="enquiry-form-input">
					<input type="text" class="form-control" placeholder="email id">
				</div>
				<div class="enquiry-form-input">
					<input type="text" class="form-control" placeholder="class">
				</div>
				<div class="enquiry-form-input">
					<input type="text" class="form-control" placeholder="subject">
				</div>
				<div class="enquiry-form-input">
					<button class="white-btn" type="submit">submit</button>
				</div>
			</form>
			 <div class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></div>
	    </div>
  	</div>
</div>
<!-- Form Modal Box -->

@include ('layouts.scripts')

</body>

</html>