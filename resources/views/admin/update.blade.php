@extends ('layouts.master')

@section ('content')

<br> <br>
<h1>   update post  </h1>
<br>
<hr>


<div class="row">
<div class="col-sm-6">


<form method="post"   action="/edit/{{$post->id}}">
{{ csrf_field() }}

  <div class="form-group">
    <label for="title">Title </label>
    <input type="text" class="form-control" id="title" name="title"  value="{{$post->title}}">
  </div>
  <div class="form-group">
    <label for="body"> Body </label>
<textarea id="body" name="body" class="form-control"  >  {{$post->body}}  </textarea >

  </div>
  <div class="form-group">
  <button type="submit" class="btn btn-primary">update</button>
</div>
</form>



@include('layouts.errors')


</div>

  
</div>




@endsection