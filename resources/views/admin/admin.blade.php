@extends('layouts.master')

    @section ('styles')

    <link rel="stylesheet" href="/css/app.css">

    
    @endsection



@section ('content')

<br> <br> <br><br>

@if (session('info') )

<div class="alert alert-success ">
{{session('info')}}
</div>
@endif


<div class="contaner">
        <a href="/q" class="btn btn-info pull-right" role="button">  Create an exam</a>
    <h2>  Admin panel     </h2>    
   


     <hr>
<div class="table-responsive">
        <table class="table  table-striped table-hover " >
                <thead class="">
                    <tr class="active">
                        <th> Id</th>
                        <th>Title</th>
                        <th> body</th>
                        <th>Action</th>
                    </tr>
                </thead>

                <tbody>
               @if (count($posts)>0)

               @foreach ($posts->all() as $p)



  
              
                    <tr>
                    <td> {{$p->id}}</td>
                    <td> {{$p->title}}</td>
                    <td>{{$p->body}}</td>
                    <td> 
                        <a href=" /read/{{$p->id}} " class="label label-primary"> read  </a> |
                        <a href=" {{url('/post')}}" class="label label-info"> create   </a> |
                        <a href="/update/{{$p->id}} " class="label label-success"> Update </a>|
                        <a href=" /delete/{{$p->id}}" class="label label-warning"> Delete  </a>


                    </td>
                    </tr> 
              

                    @endforeach

                    @endif
                </tbody>
            </table>
        </div>
  
  
    </div>



@endsection