@extends('layouts.master')

@section ('styles')

<link rel="stylesheet" href="/css/app.css">


@endsection



@section ('content')

<br> <br> <br>




<main class="main-content section-padding white-bg">
	<div class="container">

		<!-- Blog Full Width -->
		<div class="Blog-full-width">			

			<!-- Blogs Img Post -->
			<div class="full-width-post-holder">
				<div class="row">

					<!-- Post Img -->
					<div class="col-sm-12">
						<figure class="post-img clearfix">
							<img src="{{asset('laraimage/' . $post->image)}}" alt="img-01">


							{{--  do this bullshit later  --}}
							<strong class="title-batch-left"><i class="fa fa-image"></i></strong>
						</figure>
					</div>
					<!-- Post Img -->

					<!-- Post Detail -->
					<div class="col-sm-12">
						<div class="blog-post-detail">
							<div class="center-detail-inner">
								<h3>{{$post->title}}</h3>
								<ul class="meta-post">
									<li><i class="fa fa-user"></i>{{$post->user->name}}</li>
									<li><i class="fa fa-clock-o"></i>{{$post->created_at->toFormattedDateString()}}</li>
									<li><i class="fa fa-comment"></i>10 Comments</li>
								</ul>
								<p> {{$post->body}}</p>
								{{-- // get the no of comments --}}

								
							</div>

							<div class="row">
									<hr class="col-md-11">
								  </div>
						

							 
							<div class="comment">
								<ul class="">
									@foreach ($post->comments as $comment)
								 
								 <li class="list group item">
										<h4>
												{{ $comment-> created_at-> diffForHumans()}} : &nbsp;
											  {{$comment->body }}
											 </h4>
								 
								 
								 </li>
								
								 @endforeach
								</ul>
										 </div>


										 


										 
							
						</div>
						

						@if (Auth::check()) 


						<hr  class="col-md-11"> 
    
						<form method="POST" action="/p/{{$post->id}}/comments" class="col-md-11"> 
						{{csrf_field()}}
						<div class="form-group">
						
						
						<textarea name=" body" id=""   placeholder=" comment here" class="form-control" required></textarea>
						
						
						
						</div>
						
						
						<div class="form-group">
						
						<button type="submit" class="btn btn-primary">comment</button>
						</div>
						
						 </form>

@else
		

@endif


					</div>
					<!-- Post Detail -->

				</div>
				
			</div>
			

		
		<!-- Blog Full Width -->

	</div>
	<div class="row">
		<div class="col-md-11">

				@include('layouts.errors')

		</div>
</div>
</main>








@endsection





