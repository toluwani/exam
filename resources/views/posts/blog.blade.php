

@include ("layouts.head")

<body>

<!-- Wrapper -->
<div class="wrapper">
	
	<!-- Header -->
	
	
		@include('layouts.nav')
	<!-- Header -->
<br> <br>

	<!-- Banner Slider -->
	<div class="section-padding overlay-gray" data-enllax-ratio="-.3" style="background: url(images/inner-banner.jpg) 40% 0% no-repeat fixed;">
		<div class="container p-relative">

			<!-- Page heading -->
			<div class="page-heading">
				<h2>  <strong> welcome to our blog </strong></h2>
			</div>
			<!-- Page heading -->

			{{-- <!-- Bredrcum -->
			<div class="tc-bredcrum">
				
			</div> --}}
			<!-- Bredrcum -->

		</div>
	</div>
	<!-- Banner Slider -->


	@foreach  ($posts as $post)

	@include('posts.article')

	@endforeach
<br>
@include('layouts.footer')


@include('layouts.scripts')
</body>

</html>