<!-- Main Content -->
	<main class="main-content section-padding white-bg">
		<div class="container">

			<!-- Blog Full Width -->
			<div class="Blog-full-width">			

				<!-- Blogs Img Post -->
				<div class="full-width-post-holder">
					<div class="row">

						<!-- Post Img -->
						<div class="col-sm-12">
							<figure class="post-img">
								<img src="{{asset('laraimage/' . $post->image)}}" alt="img-01">
								<strong class="title-batch-left"><i class="fa fa-image"></i></strong>
							</figure>
						</div>
						<!-- Post Img -->

						<!-- Post Detail -->
						<div class="col-sm-12">
							<div class="blog-post-detail">
								<div class="center-detail-inner">
									<h3>{{$post->title}}</h3>
									<ul class="meta-post">
										<li><i class="fa fa-user"></i> {{$post->user->name}} </li>
										<li><i class="fa fa-clock-o"></i>{{$post->created_at->toFormattedDateString()}}</li>
										<li><i class="fa fa-comment"></i>10 Comments</li>
									</ul>
									<p> {{$post->body}} </p>
									<a class="pink-btn" href="/p/{{$post->id}}">read more</a>
								</div>
							</div>
						</div>
						<!-- Post Detail -->

					</div>
				</div>
				<!-- Blogs Img Post -->

			
				
			<!-- Blog Full Width -->

		</div>
	</main>
	<!-- Main Content -->
{{-- //the next thing to do is that p/1 id bullsit fix it tutorial 13 rendering posts --}}
