

@include ("layouts.head")

<body>

<!-- Wrapper -->
<div class="wrapper">
	
	<!-- Header -->
	
	
		@include('layouts.nav')
	<!-- Header -->


	<!-- Banner Slider -->
	<div class="section-padding overlay-gray" data-enllax-ratio="-.3" style="background: url(images/inner-banner.jpg) 40% 0% no-repeat fixed;">
		<div class="container p-relative">

			<!-- Page heading -->
			<div class="page-heading">
				<h2>  <strong> welcome to our blog </strong></h2>
			</div>
			<!-- Page heading -->

			<!-- Bredrcum -->
			<div class="tc-bredcrum">
				
			</div>
			<!-- Bredrcum -->

		</div>
	</div>
	<!-- Banner Slider -->

	<!-- Main Content -->
	<main class="main-content section-padding white-bg">
		<div class="container">

			<!-- Blog Full Width -->
			<div class="Blog-full-width">			

				<!-- Blogs Img Post -->
				<div class="full-width-post-holder">
					<div class="row">

						<!-- Post Img -->
						<div class="col-sm-12">
							<figure class="post-img">
								<img src="images/blog-full-width/img-01.jpg" alt="img-01">
								<strong class="title-batch-left"><i class="fa fa-image"></i></strong>
							</figure>
						</div>
						<!-- Post Img -->

						<!-- Post Detail -->
						<div class="col-sm-12">
							<div class="blog-post-detail">
								<div class="center-detail-inner">
									<h3>Autoplay image slider post</h3>
									<ul class="meta-post">
										<li><i class="fa fa-user"></i>My Admin</li>
										<li><i class="fa fa-clock-o"></i>6 days ago</li>
										<li><i class="fa fa-comment"></i>10 Comments</li>
									</ul>
									<p>Nunc ullamcorper augue nec accumsan porta. Ut lacinia fgiat viverra. Ut dictum turpis in ipsum sagittis<span>finibus. Vestibulum ut molestie lectus, id tincidunt turpis.</span></p>
									<a class="pink-btn" href="#">read more</a>
								</div>
							</div>
						</div>
						<!-- Post Detail -->

					</div>
				</div>
				<!-- Blogs Img Post -->

				<!-- Blogs Slider Post -->
				<div class="full-width-post-holder">
					<div class="row">

						<!-- Post Img Slider -->
						<div class="col-sm-12">
							<figure class="post-img">
								<ul id="slider-post" class="slider-post slider-left-batch">
									<li><img src="images/blog-full-width/img-02.jpg" alt="img-02"></li>
									<li><img src="images/blog-full-width/img-02.jpg" alt="img-02"></li>
									<li><img src="images/blog-full-width/img-02.jpg" alt="img-02"></li>
								</ul>
							</figure>
						</div>
						<!-- Post Img Slider -->

						<!-- Post Detail -->
						<div class="col-sm-12">
							<div class="blog-post-detail">
								<div class="center-detail-inner">
									<h3>Autoplay image slider post</h3>
									<ul class="meta-post">
										<li><i class="fa fa-user"></i>My Admin</li>
										<li><i class="fa fa-clock-o"></i>6 days ago</li>
										<li><i class="fa fa-comment"></i>10 Comments</li>
									</ul>
									<p>Nunc ullamcorper augue nec accumsan porta. Ut lacinia fgiat viverra. Ut dictum turpis in ipsum sagittis<span>finibus. Vestibulum ut molestie lectus, id tincidunt turpis.</span></p>
									<a class="pink-btn" href="#">read more</a>
								</div>
							</div>
						</div>
						<!-- Post Detail -->

					</div>
				</div>
				<!-- Blogs Slider Post -->	

				<!-- Blogs Attach Post -->
				<div class="full-width-post-holder">
					<div class="row">

						<!-- Post Img -->
						<div class="col-sm-12">
							<figure class="post-img">
								<img src="images/blog-full-width/img-03.jpg" alt="img-03">
								<strong class="title-batch-left"><i class="fa fa-link"></i></strong>
							</figure>
						</div>
						<!-- Post Img -->

						<!-- Post Detail -->
						<div class="col-sm-12">
							<div class="blog-post-detail">
								<div class="center-detail-inner">
									<h3>Autoplay image slider post</h3>
									<ul class="meta-post">
										<li><i class="fa fa-user"></i>My Admin</li>
										<li><i class="fa fa-clock-o"></i>6 days ago</li>
										<li><i class="fa fa-comment"></i>10 Comments</li>
									</ul>
									<p>Nunc ullamcorper augue nec accumsan porta. Ut lacinia fgiat viverra. Ut dictum turpis in ipsum sagittis<span>finibus. Vestibulum ut molestie lectus, id tincidunt turpis.</span></p>
									<a class="pink-btn" href="#">read more</a>
								</div>
							</div>
						</div>
						<!-- Post Detail -->

					</div>
				</div>
				<!-- Blogs Attach Post -->

				<!-- Blogs Video Post -->
				<div class="full-width-post-holder">
					<div class="row">

						<!-- Post Video -->
						<div class="col-sm-12">
							<div class="post-img">
								<strong class="title-batch-left"><i class="fa fa-video-camera"></i></strong>
								<img src="images/blog-full-width/img-04.jpg" alt="img-04">
								<div class="video-title-holder animate fadeInUp" data-wow-delay="0.4s">
									<div class="video-titel">
										<div class="youtube" id="lR4tJr7sMPM-3">
											<div class="position-center-center">
												<i class="play-btn fa fa-play"></i>
												<h3>School Classroom Video</h3>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- Post Video -->

						<!-- Post Detail -->
						<div class="col-sm-12">
							<div class="blog-post-detail">
								<div class="center-detail-inner">
									<h3>Autoplay image slider post</h3>
									<ul class="meta-post">
										<li><i class="fa fa-user"></i>My Admin</li>
										<li><i class="fa fa-clock-o"></i>6 days ago</li>
										<li><i class="fa fa-comment"></i>10 Comments</li>
									</ul>
									<p>Nunc ullamcorper augue nec accumsan porta. Ut lacinia fgiat viverra. Ut dictum turpis in ipsum sagittis<span>finibus. Vestibulum ut molestie lectus, id tincidunt turpis.</span></p>
									<a class="pink-btn" href="#">read more</a>
								</div>
							</div>
						</div>
						<!-- Post Detail -->

					</div>
				</div>
				<!-- Blogs Video Post -->	

				<!-- Audio Post -->
				<div class="full-width-post-holder">
					<div class="row no-gutters">
						
						<!-- Audio -->
						<div class="col-sm-12">
							<figure class="post-audio">
								<iframe src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/11213305&amp;color=f3f3f3&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false"></iframe>
								<strong class="title-batch-left"><i class="fa fa-image"></i></strong>
							</figure>
						</div>
						<!-- Audio -->

						<!-- Post Detail -->
						<div class="col-sm-12">
							<div class="blog-post-detail">
								<div class="center-detail-inner">
									<h3>Autoplay image slider post</h3>
									<ul class="meta-post">
										<li><i class="fa fa-user"></i>My Admin</li>
										<li><i class="fa fa-clock-o"></i>6 days ago</li>
										<li><i class="fa fa-comment"></i>10 Comments</li>
									</ul>
									<p>Nunc ullamcorper augue nec accumsan porta. Ut lacinia fgiat viverra. Ut dictum turpis in ipsum sagittis<span>finibus. Vestibulum ut molestie lectus, id tincidunt turpis.</span></p>
									<a class="pink-btn" href="#">read more</a>
								</div>
							</div>
						</div>
						<!-- Post Detail -->

					</div>
				</div>
				<!-- Audio Post -->
					
			</div>
			<!-- Blog Full Width -->

		</div>
	</main>
	<!-- Main Content -->

	<!-- Footer -->
	<footer id="footer" class="footer overlay-dark">
		<div class="container">
			
			<!-- Footer Logo Section -->
			<div class="footer-logo-sec">
				<div class="footer-logo-inner">
					<a class="footer-logo" href="#"><img src="images/logo.png" alt="logo"></a>
					<p>Cras pharetra justo sed scelerisque finibus. Vestibulum ante ipsum primis in faucibus
					<span>orci luctus et ultrices posuere cubilia Curae; Etiam luctus.</span></p>
					<ul class="social-icons">
						<li><a href="#"><i class="fa fa-facebook"></i></a></li>
						<li><a href="#"><i class="fa fa-twitter"></i></a></li>
						<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
						<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
						<li><a href="#"><i class="fa fa-skype"></i></a></li>
					</ul>
				</div>
			</div>
			<!-- Footer Logo Section -->

			<!-- Footer Columns -->
			<div class="row">
				<div class="footer-columns">
					
					<!-- Footer Column -->
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 r-full-width">
						<div class="footer-column">
							<h4>Contact :</h4>
							<div class="contact-widget">
								<div class="location">
									<span class="location-icon"><i class="fa fa-map-marker"></i></span>
									<div>
										<p>King Street, Melbourne</p>
										<p>VIC 3000, Australia84</p>
									</div>
								</div>
							</div>
							<div class="contact-widget">
								<div class="location">
									<span class="location-icon"><i class="fa fa-mobile"></i></span>
									<div>
										<p>King Street, Melbourne</p>
										<p>VIC 3000, Australia84</p>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- Footer Column -->

					<!-- Footer Column -->
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 r-full-width">
						<div class="footer-column">
							<h4>Recent Work :</h4>
							<ul class="flicker-img">
								<li><a href="#"><img class="animate fadeInUp" data-wow-delay="0.2s" src="images/flicker-img/img-01.jpg" alt="img-01"></a></li>
								<li><a href="#"><img class="animate fadeInUp" data-wow-delay="0.4s" src="images/flicker-img/img-02.jpg" alt="img-02"></a></li>
								<li><a href="#"><img class="animate fadeInUp" data-wow-delay="0.6s" src="images/flicker-img/img-03.jpg" alt="img-03"></a></li>
								<li><a href="#"><img class="animate fadeInUp" data-wow-delay="0.8s" src="images/flicker-img/img-04.jpg" alt="img-04"></a></li>
								<li><a href="#"><img class="animate fadeInUp" data-wow-delay="1s" src="images/flicker-img/img-05.jpg" alt="img-05"></a></li>
								<li><a href="#"><img class="animate fadeInUp" data-wow-delay="1.2s" src="images/flicker-img/img-06.jpg" alt="img-06"></a></li>
							</ul>
						</div>
					</div>
					<!-- Footer Column -->

					<!-- Footer Column -->
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 r-full-width">
						<div class="footer-column">
							<h4>School Time :</h4>
							<ul class="school-timing">
								<li>Praesent vel egestas orci.</li>
								<li>Fusccinia luctus sapien nec vulputate.</li>
								<li>School Time</li>
								<li>MON - THU :  8:00 AM To 5:00 PM</li>
								<li>FRI - SAT : 8:00 AM To 2:00 PM</li>
							</ul>
						</div>
					</div>
					<!-- Footer Column -->

					<!-- Footer Column -->
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 r-full-width">
						<div class="footer-column">
							<h4>Location :</h4>
							<div id="location-map" class="location-map"></div>
						</div>
					</div>
					<!-- Footer Column -->

				</div>
			</div>
			<!-- Footer Columns -->

			<!-- Copy Rights -->
			<div class="copy-rights">
				<p>Copyright ©2016 FineLayers</p>
			</div>
			<!-- Copy Rights -->

		</div>
	</footer>
	<!-- Footer -->

</div>
<!-- Wrapper -->

<!-- back To Button -->
<span id="scrollup" class="scrollup"><i class="fa fa-angle-up"></i></span>
<!-- back To Button -->

<!-- Form Modal Box -->
<div class="modal fade bs-example-modal-sm">
  	<div class="modal-dialog modal-sm">
	    <div class="modal-body enquiry-form style-2 white-heading">
	    	<h3>Enquiry Form</h3>
	       <form class="enquiry-form">
				<div class="enquiry-form-input">
					<input type="text" class="form-control" placeholder="full name">
				</div>
				<div class="enquiry-form-input">
					<input type="text" class="form-control" placeholder="email id">
				</div>
				<div class="enquiry-form-input">
					<input type="text" class="form-control" placeholder="class">
				</div>
				<div class="enquiry-form-input">
					<input type="text" class="form-control" placeholder="subject">
				</div>
				<div class="enquiry-form-input">
					<button class="white-btn" type="submit">submit</button>
				</div>
			</form>
			 <div class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></div>
	    </div>
  	</div>
</div>
<!-- Form Modal Box -->



@include('layouts.scripts')
</body>

</html>