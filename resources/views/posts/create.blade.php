@extends ('layouts.master')

@section ('content')

<br> <br>
<h1>   Publish a post  </h1>
<br>
<hr>


<div class="row">
<div class="col-sm-6">


<form method="post"   action="/c"  enctype="multipart/form-data">
{{ csrf_field() }}

  <div class="form-group">
    <label for="title">Title </label>
    <input type="text" class="form-control" id="title" name="title" >
  </div>
  <div class="form-group">
    <label for="body"> Body </label>
<textarea id="body" name="body" class="form-control"  > </textarea >

  </div>


  <div class="form-group">
    <div class="input-group input-file" name="image">
      <span class="input-group-btn">
            <button class="btn btn-default btn-choose" type="button">Choose</button>
        </span>
        <input type="text" class="form-control" placeholder='Choose a file...'    name="image"/ >
        <span class="input-group-btn">
              {{-- <button class="btn btn-warning btn-reset" type="button">Reset</button> --}}
        </span>
    </div>
  </div>

  <!-- COMPONENT END -->
  <div class="form-group">
    <button type="submit" class="btn btn-primary pull-right" >  Publish </button>
    <button type="reset" class="btn btn-danger">Reset</button>
  </div>






  

  

</form>



@include('layouts.errors')


</div>

  
</div>





@endsection