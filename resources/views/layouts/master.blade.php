






@include ('layouts.head')



	

@yield ('styles')






<!-- JavaScripts -->
<script src="js/vendors/modernizr.html"></script>
<script src=" /js/app.js">     </script>

</head>






<body id="selectp">

<!-- Wrapper -->
<div class="wrapper">
	
	<!-- Header -->
@include('layouts.nav')

<div class="container">

@yield ('content')


</div>



@yield ('scripts ')

@include ('layouts.scripts')


</body>

</html>



