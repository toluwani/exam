@if (count($errors))
<div class="form-group">

        <div class="alert alert-danger">


                <ul> 
                
                @foreach ($errors->all() as $er)
                
                <li>  {{$er }} </li>
                @endforeach
                </ul>
                    
                </div>

</div> 
@endif