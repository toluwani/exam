<footer id="footer" class="footer overlay-dark">
    <div class="container">
        
        <!-- Footer Logo Section -->
        <div class="footer-logo-sec">
            <div class="footer-logo-inner">
                <a class="footer-logo" href="#"><img src="images/logo.png" alt="logo"></a>
                <p>Cras pharetra justo sed scelerisque finibus. Vestibulum ante ipsum primis in faucibus
                <span>orci luctus et ultrices posuere cubilia Curae; Etiam luctus.</span></p>
                <ul class="social-icons">
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                    <li><a href="#"><i class="fa fa-skype"></i></a></li>
                </ul>
            </div>
        </div>
        <!-- Footer Logo Section -->

        <!-- Footer Columns -->
        <div class="row">
            <div class="footer-columns">
                
                <!-- Footer Column -->
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 r-full-width">
                    <div class="footer-column">
                        <h4>Contact :</h4>
                        <div class="contact-widget">
                            <div class="location">
                                <span class="location-icon"><i class="fa fa-map-marker"></i></span>
                                <div>
                                    <p>King Street, Melbourne</p>
                                    <p>VIC 3000, Australia84</p>
                                </div>
                            </div>
                        </div>
                        <div class="contact-widget">
                            <div class="location">
                                <span class="location-icon"><i class="fa fa-mobile"></i></span>
                                <div>
                                    <p>King Street, Melbourne</p>
                                    <p>VIC 3000, Australia84</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Footer Column -->

                <!-- Footer Column -->
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 r-full-width">
                    <div class="footer-column">
                        <h4>Recent Work :</h4>
                        <ul class="flicker-img">
                            <li><a href="#"><img class="animate fadeInUp" data-wow-delay="0.2s" src="images/flicker-img/img-01.jpg" alt="img-01"></a></li>
                            <li><a href="#"><img class="animate fadeInUp" data-wow-delay="0.4s" src="images/flicker-img/img-02.jpg" alt="img-02"></a></li>
                            <li><a href="#"><img class="animate fadeInUp" data-wow-delay="0.6s" src="images/flicker-img/img-03.jpg" alt="img-03"></a></li>
                            <li><a href="#"><img class="animate fadeInUp" data-wow-delay="0.8s" src="images/flicker-img/img-04.jpg" alt="img-04"></a></li>
                            <li><a href="#"><img class="animate fadeInUp" data-wow-delay="1s" src="images/flicker-img/img-05.jpg" alt="img-05"></a></li>
                            <li><a href="#"><img class="animate fadeInUp" data-wow-delay="1.2s" src="images/flicker-img/img-06.jpg" alt="img-06"></a></li>
                        </ul>
                    </div>
                </div>
                <!-- Footer Column -->

                <!-- Footer Column -->
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 r-full-width">
                    <div class="footer-column">
                        <h4>School Time :</h4>
                        <ul class="school-timing">
                            <li>Praesent vel egestas orci.</li>
                            <li>Fusccinia luctus sapien nec vulputate.</li>
                            <li>School Time</li>
                            <li>MON - THU :  8:00 AM To 5:00 PM</li>
                            <li>FRI - SAT : 8:00 AM To 2:00 PM</li>
                        </ul>
                    </div>
                </div>
                <!-- Footer Column -->

                <!-- Footer Column -->
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 r-full-width">
                    <div class="footer-column">
                        <h4>Location :</h4>
                        <div id="location-map" class="location-map"></div>
                    </div>
                </div>
                <!-- Footer Column -->

            </div>
        </div>
        <!-- Footer Columns -->

        <!-- Copy Rights -->
        <div class="copy-rights">
            <p>Copyright ©2016 FineLayers</p>
        </div>
        <!-- Copy Rights -->

    </div>
</footer>
<!-- Footer -->

</div>
<!-- Wrapper -->

<!-- back To Button -->
<span id="scrollup" class="scrollup"><i class="fa fa-angle-up"></i></span>
<!-- back To Button -->

<!-- Form Modal Box -->
<div class="modal fade bs-example-modal-sm">
  <div class="modal-dialog modal-sm">
    <div class="modal-body enquiry-form style-2 white-heading">
        <h3>Enquiry Form</h3>
       <form class="enquiry-form">
            <div class="enquiry-form-input">
                <input type="text" class="form-control" placeholder="full name">
            </div>
            <div class="enquiry-form-input">
                <input type="text" class="form-control" placeholder="email id">
            </div>
            <div class="enquiry-form-input">
                <input type="text" class="form-control" placeholder="class">
            </div>
            <div class="enquiry-form-input">
                <input type="text" class="form-control" placeholder="subject">
            </div>
            <div class="enquiry-form-input">
                <button class="white-btn" type="submit">submit</button>
            </div>
        </form>
         <div class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></div>
    </div>
  </div>
</div>
<!-- Form Modal Box -->
