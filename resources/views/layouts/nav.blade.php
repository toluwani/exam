<header id="header" class="main-header sticky-1" style="background-color:rgba(0,0,0,0.6);">
    <div class="container">
        
        <!-- logo -->
        <div class="logo-holder">
            <strong><a href="index-2.html"><img src="/images/logo.png" alt="logo"></a></strong>
        </div>
        <!-- logo -->

        <!-- Navigation -->
        <nav class="navigation-holder">
            <a class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <i class="fa fa-bars"></i> <span class="caret"></span>
            </a>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                  <ul class="tc-navigation "> 
                        @if (Auth::check())  
                            
                        <li class="">

                                <a href="{{"/"}}">Home</a>
                                
                          </li>

                          <li>
                                <a href="{{"/b"}}">blog</a>
                                
                          </li>


                        <li>
                                <a href="#"> Questions</a>
                                <ul>
                                  <li><a href="#">Graduate Degrees</a></li>
                                  <li><a href="#">Master Degrees</a></li>
                                  <li><a href="#">Bachelor  Degrees</a></li>
                                  <li><a href="#">Associate Degrees</a></li>
                                  <li><a href="#">Professional Degrees</a></li>
                                      
                                  
                                  <li><a href="#">Doctoral Degrees</a></li>
                                </ul>
                          </li>
                        

                          <li>
                                <a href="#.">contact</a>
                                
                          </li>
                          <li>
                                <a href="{{"/logout"}}">logout</a>
                                
                          </li>
                     
                    @else
                    <li class="">

                            <a href="{{"/"}}">Home</a>
                            
                      </li>
                        <li>
                            <a href="{{"/b"}}">blog</a>
                            
                      </li>

                      
                     
                      <li>
                          <a href="{{"/l"}}">login</a>
                          
                    </li>
                    <li>
                            <a href="{{"/r"}}"> sign up</a>
                            
                      </li>
                    <li>
                          <a href="#.">contact</a>
                          
                    </li>
                    
                    
                    @endif

                  </ul>
            </div>
        </nav>
        <!-- Navigation -->

    

    </div>
   
</header>

