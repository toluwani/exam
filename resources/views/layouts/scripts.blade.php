<!-- Java Script -->
<script src="/js/vendor/jquery.js"></script>        		
<script src="/js/vendor/bootstrap.js"></script> 	
<script src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script src="/js/gmap3.min.js"></script>				
<script src="/js/parallax.js"></script>			   	 
<script src="/js/contact-form.js"></script>			   	 
<script src="/js/tabulous.html"></script>			   	 
<script src="/js/countdown.js"></script>	
<script src="/js/owl-carousel.js"></script>
<script src="/js/nstslider.js"></script>
<script src="/js/odometer.js"></script>					
<script src="/js/classie.js"></script>			
<script src="/js/bootstrap-select.js"></script>				
<script src="/js/colorpicker.js"></script>					
<script src="/js/appear.js"></script>		
<script src="/js/prettyPhoto.js"></script>			
<script src="/js/isotope.pkgd.js"></script>						
<script src="/js/sticky.js"></script>						
<script src="/js/wow-min.js"></script>

    <script src="/js/main.js"></script>		
<!-- Java Script -->						
