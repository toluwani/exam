<!doctype html>
<html class="no-js" lang="en">

<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="author" content=""/>
<!-- Document Title -->
<title> Exam portal </title>

<!-- StyleSheets -->
<link rel="stylesheet" href="/css/vendor/bootstrap.css">
<link rel="stylesheet" href="/css/style.css">
<link rel="stylesheet" href="/css/icomoon.css">
<link rel="stylesheet" href="/css/main.css">
<link rel="stylesheet" href="/css/color-1.css">
<link rel="stylesheet" href="/css/animate.css">
<link rel="stylesheet" href="/css/font-awesome.min.css">
<link rel="stylesheet" href="/css/responsive.css">
<link rel="stylesheet" href="/css/transition.css">
<link rel="stylesheet" href="/css/app.css">




<!-- FontsOnline -->
{{-- <link href="https://fonts.googleapis.com/css?family=Mrs+Saint+Delafield|Roboto:300,300i,400,400i,500,500i,700,700i,900,900i|Source+Sans+Pro:300,300i,400,400i,600,600i,700,700i,900,900i" rel="stylesheet"> --}}

<!-- JavaScripts -->
{{-- <script src="/js/vendors/modernizr.html"></script> --}}
<script src=" /js/app.js">     </script>



</head>
