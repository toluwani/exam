<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


Route::get('/', function () {
    return view(' home');
})->name('home');


Route::get('/rt', function () {
    return view(' posts.index');
});


 





//signup routes
Route:: get('/r','authcontroller@getsignup');
Route:: post('/r','authcontroller@postsignup');

//signin routes

Route:: get('/l','authcontroller@getsigin');
Route:: post('/l','authcontroller@postsigin');


Route:: get('/profile','authcontroller@getprofile');

//logout route
Route::get('/logout','authcontroller@destroy')->middleware('auth');


//post routes

Route:: get('/b','postscontroller@index');
Route:: get('/p/{post}','postscontroller@show');
Route:: post('/c','postscontroller@store')->middleware('auth');
Route:: get('/post','postscontroller@create')->middleware('auth');

//comment controller
Route::post('/p/{post}/comments','commentcontroller@store');


// admincontroller
Route:: get('/admin','admincontroller@index')->name('admin');;
Route:: get('/update/{id}','admincontroller@update');

Route:: post('/edit/{id}','admincontroller@edit');
Route:: get('/read/{id}','admincontroller@read');
Route:: get('/delete/{id}','admincontroller@delete');

//questioncontroller

Route:: get('/exam','questioncontroller@index');
Route:: get('/q','questioncontroller@show');
Route:: post('/q','questioncontroller@store');
